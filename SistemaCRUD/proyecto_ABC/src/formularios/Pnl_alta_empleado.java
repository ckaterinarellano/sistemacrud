package formularios;

import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class Pnl_alta_empleado extends JPanel {
	private JPanel panel;
	private JLabel lblAltaNuevo;
	private JPanel panel_1;
	private JLabel lblTítulo2;
	private JLabel lblCurp;
	private JLabel lblNombre;
	private JLabel lblApellidoPaterno;
	private JLabel lblApellidoMaterno;
	private JLabel lblDomicilio;
	private JLabel lblAñoNacimiento;
	private JLabel lblNewLabel_8;
	private JLabel lblNewLabel_9;
	private JTextField textCurp;
	private JTextField textNombre;
	private JTextField textDomicilio;
	private JTextField textApellidoPaterno;
	private JTextField textApellidoMaterno;
	private JTextField textContraseña;
	private JComboBox cboAñoNacimiento;
	private JComboBox cboArea;
	private JPanel panel_2;
	private JLabel lblNewLabel_10;
	private JLabel lblNewLabel_11;
	private JLabel lblNewLabel_12;
	private JLabel lblNewLabel_13;
	private JButton btnGuardar;
	private JButton btnBorrar;
	private JButton btnContraseñaAleatoria;

	/**
	 * Create the panel.
	 */
	public Pnl_alta_empleado() {
		setLayout(null);
		
		panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setForeground(Color.LIGHT_GRAY);
		panel.setBounds(0, 0, 851, 597);
		add(panel);
		panel.setLayout(null);
		
		lblAltaNuevo = new JLabel("Alta de nuevo empleado");
		lblAltaNuevo.setBounds(22, 13, 411, 56);
		lblAltaNuevo.setFont(new Font("Tahoma", Font.BOLD, 24));
		panel.add(lblAltaNuevo);
		
		panel_1 = new JPanel();
		panel_1.setBounds(32, 71, 640, 513);
		panel_1.setBackground(Color.WHITE);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		lblTítulo2 = new JLabel("Inserte datos del empleado");
		lblTítulo2.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblTítulo2.setBounds(12, 13, 393, 36);
		panel_1.add(lblTítulo2);
		
		lblCurp = new JLabel("CURP:");
		lblCurp.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblCurp.setBounds(12, 80, 56, 16);
		panel_1.add(lblCurp);
		
		lblNombre = new JLabel("Nombre:");
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNombre.setBounds(12, 132, 68, 16);
		panel_1.add(lblNombre);
		
		lblApellidoPaterno = new JLabel("Apellido Paterno:");
		lblApellidoPaterno.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblApellidoPaterno.setBounds(12, 178, 142, 16);
		panel_1.add(lblApellidoPaterno);
		
		lblApellidoMaterno = new JLabel("Apellido Materno:");
		lblApellidoMaterno.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblApellidoMaterno.setBounds(12, 229, 142, 16);
		panel_1.add(lblApellidoMaterno);
		
		lblDomicilio = new JLabel("Domicilio:");
		lblDomicilio.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblDomicilio.setBounds(12, 267, 68, 16);
		panel_1.add(lblDomicilio);
		
		lblAñoNacimiento = new JLabel("A\u00F1o de nacimiento:");
		lblAñoNacimiento.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblAñoNacimiento.setBounds(12, 318, 142, 16);
		panel_1.add(lblAñoNacimiento);
		
		lblNewLabel_8 = new JLabel("Area:");
		lblNewLabel_8.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_8.setBounds(411, 229, 56, 16);
		panel_1.add(lblNewLabel_8);
		
		lblNewLabel_9 = new JLabel("Contrase\u00F1a:");
		lblNewLabel_9.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_9.setBounds(400, 272, 85, 16);
		panel_1.add(lblNewLabel_9);
		
		textCurp = new JTextField();
		textCurp.setBounds(159, 77, 215, 24);
		panel_1.add(textCurp);
		textCurp.setColumns(10);
		
		textNombre = new JTextField();
		textNombre.setColumns(10);
		textNombre.setBounds(159, 129, 219, 24);
		panel_1.add(textNombre);
		
		textDomicilio = new JTextField();
		textDomicilio.setColumns(10);
		textDomicilio.setBounds(159, 264, 219, 24);
		panel_1.add(textDomicilio);
		
		textApellidoPaterno = new JTextField();
		textApellidoPaterno.setColumns(10);
		textApellidoPaterno.setBounds(159, 175, 219, 24);
		panel_1.add(textApellidoPaterno);
		
		textApellidoMaterno = new JTextField();
		textApellidoMaterno.setColumns(10);
		textApellidoMaterno.setBounds(159, 226, 219, 24);
		panel_1.add(textApellidoMaterno);
		
		textContraseña = new JTextField();
		textContraseña.setColumns(10);
		textContraseña.setBounds(497, 264, 121, 24);
		panel_1.add(textContraseña);
		
		cboAñoNacimiento = new JComboBox();
		cboAñoNacimiento.setBounds(156, 316, 110, 22);
		panel_1.add(cboAñoNacimiento);
		
		cboArea = new JComboBox();
		cboArea.setBounds(497, 227, 121, 22);
		panel_1.add(cboArea);
		
		lblNewLabel_11 = new JLabel("Nombre:");
		lblNewLabel_11.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_11.setBounds(411, 81, 68, 16);
		panel_1.add(lblNewLabel_11);
		
		lblNewLabel_12 = new JLabel("Nombre:");
		lblNewLabel_12.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_12.setBounds(411, 144, 68, 16);
		panel_1.add(lblNewLabel_12);
		
		lblNewLabel_13 = new JLabel("Nombre:");
		lblNewLabel_13.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel_13.setBounds(399, 413, 68, 16);
		panel_1.add(lblNewLabel_13);
		
		btnContraseñaAleatoria = new JButton("");
		btnContraseñaAleatoria.setIcon(new ImageIcon("C:\\sistemacrud\\SistemaCRUD\\proyecto_ABC\\img\\random.png"));
		btnContraseñaAleatoria.setBounds(509, 316, 97, 50);
		panel_1.add(btnContraseñaAleatoria);
		
		panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		panel_2.setBounds(684, 71, 155, 513);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		lblNewLabel_10 = new JLabel("Opciones");
		lblNewLabel_10.setBounds(12, 13, 56, 16);
		panel_2.add(lblNewLabel_10);
		
		btnGuardar = new JButton("");
		btnGuardar.setBounds(32, 54, 97, 51);
		panel_2.add(btnGuardar);
		
		btnBorrar = new JButton("");
		btnBorrar.setIcon(new ImageIcon("C:\\sistemacrud\\SistemaCRUD\\proyecto_ABC\\img\\borrar.png"));
		btnBorrar.setBounds(32, 160, 97, 78);
		panel_2.add(btnBorrar);

	}
	
	public void imagen_gif_guardar() {
		ImageIcon check_gif = new ImageIcon(getClass().getResource("C:\\sistemacrud\\SistemaCRUD\\proyecto_ABC\\img\\check.gif"));
		Icon gif = new ImageIcon(check_gif.getImage().getScaledInstance(100,100,Image.SCALE_DEFAULT));
		btnGuardar.setIcon(gif);
	}
}
