package formularios;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JToggleButton;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.net.URL;
import java.awt.event.ActionEvent;

public class menu_principal extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JPanel panel;
	private JPanel pnl_vista_principal;
	private JLabel lblMenu;
	private JToggleButton tglbtnRegistroEmpleado;
	private JToggleButton tglbtnModificarEmpleado;
	private JToggleButton tglbtnEliminarEmpleado;
	private JToggleButton tglbtnBuscarEmpleado;
	private JButton btnCerrar;

	/**
	 * Launch the application.
	 */
	//IDENTIFICADORES DE CAR LAYOUT
	final static String VENTANA1 = "Ventana 1";
	
	Pnl_alta_empleado pnl_alta_empleado = new Pnl_alta_empleado();
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					menu_principal frame = new menu_principal();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public menu_principal() {
		setTitle("Sistema de CRUD de Empleados");
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\sistemacrud\\SistemaCRUD\\proyecto_ABC\\img\\icon.png"));
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 944, 671);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		/*Para maximizar el frame*/
		setExtendedState(JFrame.MAXIMIZED_BOTH);
		
		
		
		panel = new JPanel();
		panel.setBounds(12, 13, 273, 598);
		contentPane.add(panel);
		panel.setLayout(null);
		
		lblMenu = new JLabel("Men\u00FA del Sistema");
		lblMenu.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblMenu.setBounds(68, 13, 148, 16);
		panel.add(lblMenu);
		
		tglbtnRegistroEmpleado = new JToggleButton("   Registro de empleado");
		tglbtnRegistroEmpleado.addActionListener(this);
		tglbtnRegistroEmpleado.setIcon(new ImageIcon("C:\\sistemacrud\\SistemaCRUD\\proyecto_ABC\\img\\alta.png"));
		tglbtnRegistroEmpleado.setBounds(12, 42, 249, 108);
		panel.add(tglbtnRegistroEmpleado);
		
		tglbtnModificarEmpleado = new JToggleButton("Modificar empleado");
		tglbtnModificarEmpleado.setIcon(new ImageIcon("C:\\sistemacrud\\SistemaCRUD\\proyecto_ABC\\img\\modificar.png"));
		tglbtnModificarEmpleado.setBounds(12, 152, 249, 108);
		panel.add(tglbtnModificarEmpleado);
		
		tglbtnEliminarEmpleado = new JToggleButton("  Eliminar empleado");
		tglbtnEliminarEmpleado.setIcon(new ImageIcon("C:\\sistemacrud\\SistemaCRUD\\proyecto_ABC\\img\\eliminar.png"));
		tglbtnEliminarEmpleado.setSelectedIcon(new ImageIcon("D:\\SistemaCRUD\\proyecto_ABC\\src\\imagenes\\eliminar_usuario.png"));
		tglbtnEliminarEmpleado.setBounds(12, 263, 249, 108);
		panel.add(tglbtnEliminarEmpleado);
		
		tglbtnBuscarEmpleado = new JToggleButton("   Buscar empleado");
		tglbtnBuscarEmpleado.setIcon(new ImageIcon("C:\\sistemacrud\\SistemaCRUD\\proyecto_ABC\\img\\buscar.png"));
		tglbtnBuscarEmpleado.setSelectedIcon(new ImageIcon("D:\\SistemaCRUD\\proyecto_ABC\\src\\imagenes\\buscar_usuario.png"));
		tglbtnBuscarEmpleado.setBounds(12, 373, 249, 108);
		panel.add(tglbtnBuscarEmpleado);
		
		btnCerrar = new JButton("Cerrar");
		btnCerrar.addActionListener(this);
		btnCerrar.setIcon(new ImageIcon("C:\\sistemacrud\\SistemaCRUD\\proyecto_ABC\\img\\cerrar.png"));
		btnCerrar.setBounds(79, 525, 120, 60);
		panel.add(btnCerrar);
		
		pnl_vista_principal = new JPanel();
		pnl_vista_principal.setBounds(297, 13, 1021, 634);
		contentPane.add(pnl_vista_principal);
		pnl_vista_principal.setLayout(new CardLayout(0, 0));
				
	}
		
	
	
	/*public JFrame_Icon() {
		
		setTitle("Sistema CRUD");
		setBounds(200,200,300,300);
		setIconImage(Toolkit.getDefaultToolkit().
				getImage(menu_principal.class.getResource("/img/icon_system.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}*/
	
	
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == tglbtnRegistroEmpleado) {
			actionPerformedTglbtnRegistroEmpleado(arg0);
		}
		if (arg0.getSource() == btnCerrar) {
			actionPerformedBtnCerrar(arg0);
		}
	}
	protected void actionPerformedBtnCerrar(ActionEvent arg0) {
		dispose();
	}
	protected void actionPerformedTglbtnRegistroEmpleado(ActionEvent arg0) {
		
		CardLayout vista = (CardLayout)(pnl_vista_principal.getLayout());
		vista.show(pnl_vista_principal, VENTANA1);
		pnl_vista_principal.add(pnl_alta_empleado, VENTANA1);
		
	}
}
